<?php

/**
 * Administrative page callback for monitoring the state of the stored SQL procedure that claims promo codes.
 */
function promocodes_admin_page() {

}

/**
 * Batch code creation or upload form for promo codes.
 */
function promocodes_batch_form($form, $form_state) {


  return $form;
}

/**
 * Form validation handler for promo codes.
 */
function promocodes_batch_form_validate(&$form, &$form_state) {

}

/**
 * Form submit handler for promo codes.
 */
function promocodes_batch_form_submit(&$form, &$form_state) {

}

/**
 * Ajax refresh function for promo codes batch form.
 */
function promocodes_batch_form_ajax_refresh(&$form, &$form_state) {

}


