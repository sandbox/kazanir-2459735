<?php

function promocodes_drush_command() {
  $commands = [];

  $commands['promocodes-import'] = [
    'description' => 'Import a CSV of promotional codes for a specific node',
    'options' => [
      'file' => 'The file to be imported. (The first column should contain the codes.)',
      'nid' => 'The node ID the codes should be attached to.',
    ],
  ];

  return $commands;
}

function drush_promocodes_import() {
  $file = drush_get_option('file');
  if (!$file) {
    return drush_set_error("No file was provided.");
  }
  $nid = drush_get_option('nid');
  if (!$nid) {
    return drush_set_error("No promotion node was provided.");
  }
  if (!in_array(node_load($nid)->type, variable_get('promocodes_node_types', []))) {
    return drush_set_error("The node provided is not in the list of enabled node types for promotional codes.");
  }

  $fh = fopen($file, 'r');

  while (!feof($fh)) {
    $line = fgetcsv($fh, 1024);
    if (!empty($line[0])) {
      $codes[] = $line[0];
    }
  }

  promocodes_write_batch($codes, $nid);

}

