<?php

$plugin = [
  'name' => 'promotion',
  'title' => t('Promo code claim form'),
  'description' => t('Displays the claim form for a promotion node'),
  'single' => TRUE,
  'content_types' => 'promocodes_claim_form',
  'render callback' => 'promocodes_claim_pane',
  'required context' => new ctools_context_required(t('Promotion'), 'node'),
  'edit form' => 'promocodes_claim_pane_edit_form',
  'category' => [t('Node'), 30],
];

function promocodes_claim_pane_edit_form($form, &$form_state) {
  return $form;
}

function promocodes_claim_pane_edit_form_submit($form, &$form_state) {

}

function promocodes_claim_pane($subtype, $conf, $args, $contexts) {
  if ($contexts->plugin == 'entity:node'
    && in_array($contexts->data->type, variable_get('promocodes_node_types', []))
  ) {
    $rndr = new stdClass();
    $rndr->title = '';
    $rndr->content = drupal_get_form('promocodes_claim_form', $contexts->data);
    return $rndr;
  }
  else {
    $rndr = (object) ['title' => '', 'content' => []];
    return $rndr;
  }

}
