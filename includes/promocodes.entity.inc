<?php

class PromoCode extends Entity {

  public static function create($code, $nid, $status, $created, $expires) {
    $values = [
      'code' => $code,
      'nid' => $nid,
      'status' => $status,
      'created' => $created,
      'expires' => $expires,
      // Fill in the newly-created defaults.
      'type' => 'code',
      'claimed' => NULL,
      'claim_uid' => NULL,
      'claim_mail' => NULL,
      'data' => array(),
      'is_new' => TRUE,
      'id' => NULL,
    ];

    $obj = new PromoCode($values, 'promocode');
    return $obj;
  }

}
